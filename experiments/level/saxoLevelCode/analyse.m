% clear all
% close all
% clc

label={'Sound','WavForm','Spec','Spack'};

inputPath1='../saxoLevelResults1/';
inputPath2='../saxoLevelResults2/';


%% get Data
data1=getData(label,inputPath1);data2=getData(label,inputPath2);
data=[data1 data2];

plot(data')
legend({'S','W','Spe','Spa'})
mean(data, 2);

colormap(parula(2));
% map(1)=1;
% clf
% hold on
% for k=2:3
% histogram(data(k+1, :),[.5:.04:1],'facecolor',map(k,:),'facealpha',.7,'edgecolor','none')
% end

hist(data(3:4, :)', [.5:.04:1])

name = {'Spe', 'Spa'};
legend(name)
legend boxoff
xlabel('p(c)')
set(gca,'fontsize',20,'box','off');
saveas(gcf, 'levelHist', 'png')
savefig(gcf, 'levelHist')

h1 = histogram(data(3, :));
h1.BinWidth = 0.04;
h1.EdgeColor='none';

h2 = histogram(data(4, :));
h2.BinWidth = 0.04;

h2.EdgeColor='none';
name = {'Spe', 'Spa'};
for k=1:2
    h1 = histogram(data(k+2, :));
h1.BinWidth = 0.04;
axis([0.45 1.05 0 6]);

% axis tight
legend boxoff
set(gca,'fontsize',20,'box','off');
saveas(gcf, ['levelHist' name{k}], 'png')
savefig(gcf, ['levelHist' name{k}])
end

%% Boxplot
figure(1)
bh=boxplot(data');
set(bh(:,:),'Color','k','linewidth',2);
set(gca,'XTICK',[1 2 3 4],'XTICKLABEL',{'S','W','Spe','Spa'},'fontsize',20,'box','off')
axis([0.5 4.5 .4 1]);
ylabel('p(c)')
saveas(gcf, 'levelBox', 'png')
savefig(gcf, 'levelBox')
figure(2)
errorbar(mean(data,2),std(data'),'ko','linewidth',4);
axis([0.8 4.2 .4 1]);
set(gca,'XTICK',[1 2 3 4],'XTICKLABEL',{'S','W','Spe','Spa'},'fontsize',20,'box','off')
ylabel('p(c)')
saveas(gcf, 'levelResults', 'png')
savefig(gcf, 'levelResults')



%% Repeated measure ANOVA
disp('Rm ANOVA representation')

t=table(data(1,:)',data(2,:)',data(3,:)',data(4,:)','VariableNames',{'Y1','Y2','Y3','Y4'});
within = table({'Sound';'Waveform';'Spec';'Spack'},'VariableNames',{'representation'});
rm = fitrm(t,'Y1-Y4~1','WithinDesign',within);
ranovatbl = ranova(rm,'WithinModel','representation');

% mauchly
ma=mauchly(rm);

% grpstats
statstbl=grpstats(rm,{'representation'},{'mean','std','min','max','range'});

% multcompare
multtabl=multcompare(rm,'representation');

% dips
disp(statstbl);
disp(ma);
disp('')
disp(ranovatbl);
disp('')
disp(multtabl);
disp('')