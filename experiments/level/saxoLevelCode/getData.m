function [data] = getData(label,dataPath )

if nargin == 0
    getData('saxoResults');
    return;
end

files=dir([dataPath '*.mat']); 

subjectNames=unique(cellfun(@(x) x(strfind(x,'_')+1:end-4),{files.name},'uniformoutput',false));

data=zeros(length(label),length(subjectNames));

for ii=1:length(label)
    for jj=1:length(subjectNames)
        
        load([dataPath label{ii} '_' subjectNames{jj} '.mat']);
        
        ind=strcmp('high',results(:,5:8));
        gt=abs(sum(ind(:,1:3),2)-2);
        data(ii,jj)=sum(gt==ind(:,4))/size(results,1);
    end  
end
end

