clear all
close all

dataPath='saxoResults';
type={'Sound','WavForm','Spec','Spack'};

rng(0,'twister');
for ii=1:length(type)
    load([dataPath '/aurelievonsy_resultsDiscrimination_' type{ii} '.mat']);
    for jj=1:10
        tmp=cell(24,8);
        res=randi(3,24,1);
        tmp(:,1:7)=results(:,1:7);
        for nn=1:length(res)
            tmp{nn,8}=tmp{nn,res(nn)+4};
        end
        results=tmp;
        save([dataPath '/sujetMystere' num2str(jj) '_resultsDiscrimination_' type{ii} '.mat'],'results')
    end
end