function [dataDisim,dataPcClass,dataPcReco,labels] = getData( dataPath )

if nargin == 0
    getData('cleanData');
    return;
end
type={'sound','waveform','spectrogram','spack'};
dataDisim=zeros(4,8,20,20);
dataPcClass=zeros(4,8);
dataPcReco=zeros(4,8);

for xx=1:length(type)
    files=dir([dataPath '/*' type{xx} '.mat']);
    for jj=1:length(files)
        
        load([dataPath '/' files(jj).name])

        Disim = zeros(length(results),length(results));
        
        pcReco=mean(strcmp(results(:,2),results(:,5)));
        
        for nn=1:size(results,1)
            for mm=1:size(results,1)
                Disim_gt(nn,mm)=~strcmp(results{nn,2},results{mm,2});
                Disim_sujet(nn,mm)=~strcmp(results{nn,5},results{mm,5});
            end
        end
        
        labels=results(:,2);
        dataDisim(xx,jj,:,:)=Disim_sujet;
        dataPcReco(xx,jj)=pcReco;
        dataPcClass(xx,jj)=mean(squareform(Disim_gt)==squareform(Disim_sujet));
    end
end