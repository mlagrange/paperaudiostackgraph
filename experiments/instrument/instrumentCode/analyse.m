clear all
close all
clc
type={'Sound','Waveform','Spectrogram','Spack'};
[dataDisim,dataPcClass,dataPcReco,labels] = getData('../instrumentResults/instrumentSpack_mat');

%% boxplot
figure(2)
boxplot(dataPcClass');
axis([0.5 4.5 0.5 1.1]);
set(gca,'XTICK',[1 2 3 4],'xticklabel',{'S','W','Spe','Spa'},'fontsize',14,'box','off','ytick',.5:.1:1,'yticklabel',.5:.1:1)
ylabel('p(c)')
xlabel('Classification')
% 
% figure(1)
% errorbar(mean(dataPcClass,2),std(dataPcClass'),'ro');
% axis([0 5 0.5 1.1]);
% set(gca,'XTICK',[1 2 3 4],'xticklabel',{'S','W','Spe','Spa'},'fontsize',14,'box','off','ytick',.5:.1:1,'yticklabel',.5:.1:1)
% ylabel('p(c)')
% xlabel('Classification')
% disp('')

% figure(3)
% boxplot(dataPcReco');
% axis([0 5 -.1 1.1]);
% set(gca,'XTICK',[1 2 3 4],'xticklabel',{'S','W','Spe','Spa'},'fontsize',14,'box','off','ytick',0:.1:1,'yticklabel',0:.1:1)
% ylabel('p(c)')
% xlabel('Reconnaissance')
% 
% figure(4)
% errorbar(mean(dataPcReco,2),std(dataPcReco'),'ro');
% axis([0 5 -.1 1.1]);
% set(gca,'XTICK',[1 2 3 4],'xticklabel',{'S','W','Spe','Spa'},'fontsize',14,'box','off','ytick',0:.1:1,'yticklabel',0:.1:1)
% ylabel('p(c)')
% xlabel('Reconnaissance')
% disp('')

figure(1)
errorbar(mean(dataPcClass,2),std(dataPcClass'),'k*','linewidth',4,'markersize',15);
axis([0.8 4.2 0.8 1.05]);
set(gca,'XTICK',[1 2 3 4],'XTICKLABEL',{'S','W','Spe','Spa'},'fontsize',14,'box','off','ytick',0:.1:1,'yticklabel',0:.1:1)
ylabel('p(c)')
saveas(gcf, 'instrumentResults', 'png')
savefig(gcf, 'instrumentResults')
% legend({'Reconnaissance','Classification'},'location','northoutside','orientation','horizontal')
% disp('')

%% Repeated measure ANOVA
disp('Rm ANOVA representation')

t=table(dataPcClass(1,:)',dataPcClass(2,:)',dataPcClass(3,:)',dataPcClass(4,:)','VariableNames',{'Y1','Y2','Y3','Y4'});
within = table({'Sound';'Waveform';'Spec';'Spack'},'VariableNames',{'representation'});
rm = fitrm(t,'Y1-Y4~1','WithinDesign',within);
ranovatbl = ranova(rm,'WithinModel','representation');

% mauchly
ma=mauchly(rm);

% grpstats
statstbl=grpstats(rm,{'representation'},{'mean','std','min','max','range'});

% multcompare
multtabl=multcompare(rm,'representation');

% dips
disp(statstbl);
disp(ma);
disp('')
disp(ranovatbl);
disp('')
disp(multtabl);
disp('')

%% MDSCALE
close all
rng(0,'twister')



for jj=1:length(type)
    vd=squareform(squeeze(sum(dataDisim(jj,:,:,:),2)));
    vd=vd+rand(length(vd),1)'/50;
    [pos,stress]=mdscale(vd,2,'criterion','sstress');
    figure(1+jj)
    plot(pos(1:5,1),pos(1:5,2),'r*','markersize',15,'linewidth',4)
    hold on
    plot(pos(6:10,1),pos(6:10,2),'b*','markersize',15,'linewidth',4)
    plot(pos(11:15,1),pos(11:15,2),'g*','markersize',15,'linewidth',4)
    plot(pos(16:20,1),pos(16:20,2),'k*','markersize',15,'linewidth',4)
    hold off
    set(gca,'fontsize',14,'box','off','xtick',[],'xticklabel',[],'ytick',[],'yticklabel',[])
    xlabel(['mdscale stress = ' num2str(stress)])
    title(type{jj})
    if jj==3
        legend({'Piano','Trumpet','Violin','Flute'},'location','southoutside','orientation','horizontal')
    end
    disp('')
end

%% HAC
close all
for jj=1:length(type)
    Z = linkage(squeeze(sum(dataDisim(jj,:,:,:),2)),'ward');
    figure(jj)
    dendrogram(Z,'labels',labels, 'Orientation','right');
    title(type{jj})
    disp('');
end