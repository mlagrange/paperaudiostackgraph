clear all
close all
clc

outputPath='cleanData/';
inputPath='data/';

files=dir(inputPath);
files=files(3:end);

for jj=1:length(files)
    [name,gt,px,py,r,g,b]=textread([inputPath files(jj).name],'%s %s %f %f %f %f %f');
    for ii=1:length(name)
        if [r(ii) g(ii) b(ii)] == [0 0 0]
            class{ii}='piano';
        elseif [r(ii) g(ii) b(ii)] == [0.972549 0.282353 0.972549]
            class{ii}='trumpet';
        elseif [r(ii) g(ii) b(ii)] == [0.972549 0.309804 0.309804]
            class{ii} = 'violin';
        elseif [r(ii) g(ii) b(ii)] == [0.282353 0.972549 0.282353]
            class{ii} = 'flute';
        else
            error('wrong rgb input')
        end
    end
    results=cell(20,5);
    results(:,1)=name;
    results(:,2)=gt;
    for nn=1:length(px)
        results{nn,3}=px(nn);
        results{nn,4}=py(nn);
    end
    results(:,5)=class;

    save([outputPath files(jj).name '.mat'],'results')
    
end