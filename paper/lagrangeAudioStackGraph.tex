% -----------------------------------------------
% Template for ISMIR 2014
% (based on earlier ISMIR templates)
% -----------------------------------------------

\documentclass{article}
\usepackage{amsmath,cite,ismir}
\usepackage{graphicx}
\usepackage{stfloats}
\usepackage{url}
\usepackage[position=top]{subfig}
\usepackage{tikz,pgf}
\usetikzlibrary{arrows,positioning}

%\usepackage{hyperref}
%\hypersetup{
%    bookmarks=true,         % show bookmarks bar?
%    unicode=false,          % non-Latin characters in Acrobat�s bookmarks
%    pdftoolbar=true,        % show Acrobat�s toolbar?
%    pdfmenubar=true,        % show Acrobat�s menu?
%    pdffitwindow=false,     % window fit to page when opened
%    pdfstartview={FitH},    % fits the width of the page to the window
%    pdftitle={My title},    % title
%    pdfauthor={Author},     % author
%    pdfsubject={Subject},   % subject of the document
%    pdfcreator={Creator},   % creator of the document
%    pdfproducer={Producer}, % producer of the document
%    pdfkeywords={keyword1} {key2} {key3}, % list of keywords
%    pdfnewwindow=true,      % links in new window
%    colorlinks=false,       % false: boxed links; true: colored links
%    linkcolor=red,          % color of internal links (change box color with linkbordercolor)
%    citecolor=green,        % color of links to bibliography
%    filecolor=magenta,      % color of file links
%    urlcolor=cyan           % color of external links
%}

% Title.
% ------
\title{Visualization of audio data using stacked graphs}

\oneauthor
  {Mathieu Lagrange$^*$,  Mathias Rossignol, Gr\'egoire Lafay$^*$
} {$^*$LS2N, CNRS, \'Ecole Centrale de Nantes \\ {\tt mathieu.lagrange@cnrs.fr}}


\graphicspath{{figures/}{./}}

\sloppy

\renewcommand{\baselinestretch}{.99}

\begin{document}
%
\maketitle
%
\begin{abstract}

 In this paper, we study the benefit of considering stacked graphs to display audio data. Thanks to a careful use of layering of the spectral information, the resulting display is both concise and intuitive. Compared to the spectrogram display, it allows the reader to focus more on the temporal aspect of the time/frequency decomposition while keeping an abstract view of the spectral information.

 The use of such a display is validated using two perceptual experiments that demonstrate the potential of the approach. The first considers the proposed display to perform an identification task of the musical instrument and the second considers the proposed display to evaluate the technical level of a musical performer. Both experiments show the potential of the display and potential applications scenarios in musical training are discussed.
\end{abstract}

\section{Introduction}\label{sec:introduction}

The visual display of quantitative information~\cite{Tufte1983} is at the core of the growth of human knowledge as it allows human beings to go beyond the limitation of natural languages in terms of precision and scale.

Defining what is the essence of a good visual display of quantitative data is non trivial and usually domain specific. That said, in most scientific fields, such displays serve two majors goals: 1) the routine interaction of the researcher with the data or the physical phenomenon and 2) the need of the researcher to motivate its claim to its peers. Both tasks require the display to fulfill the simplicity rule both in terms of production and design. First, the display shall be computed and adapted according to the need of the researcher very efficiently in order to allow an effective exploration of the data. Second, the display shall be able to convey at the first glance an important qualitative aspect about the data.

This paper is about the visualization of audio data, and audio data is originally made to be listened to. Therefore, we shall keep in mind that "all visual projections of sounds are arbitrary and fictitious"~\cite{Schafer1977}. That said, even if recorded versions of sounds can now be played back at convenience, it is still useful to represent them graphically as listening depends on time. On contrary, the visual display allows the reader to grasp a global view of the waveform at a glance. Also, the eye is less subject to stimulation fatigue and the visual display is very powerful to convey evidence as we are still fully into the print culture that since the Gutenberg invention gives an "uncritical acceptance [to] visual metaphors and models"~\cite{McLuhan1963}. % maybe even since prehistoric cave paintings...

We propose in this paper a display of audio data that is intuitive and gives information about the main dimensions of sound in a compact manner using stacked graphs~\cite{Byron2008}. The display can be computed easily and efficiently\footnote{A reference implementation as well as all the data discussed in this paper is available at \url{https://bitbucket.org/mlagrange/paperaudiostackgraph}}.
In order to put this display into context, an overview of the routinely used type of displays is given, respectively from the perspective of the musician composer in Section~\ref{sec:notation} and the physicist in Section~\ref{sec:physicist}. We shall argue that the proposed display fully described in Section~\ref{sec:spack} can be thought of as the physicist's counterpart to a notational system introduced by Schafer~\cite{Schafer1977}.

The display is then evaluated and compared to the commonly used waveform and spectrogram displays with two perceptual experiments. In the first experiment, the subjects have to distinguish between tones of different musical instruments by listening to the sounds or by considering the visual displays under evaluation. The protocol and the results for this experiment are presented in Section~\ref{sec:instrument}. In the second experiment, the subjects are asked to distinguish between saxophone performances of different level of instrumental expertise by listening to the sound and by considering the displays under evaluation. The protocol and the results for this experiment are presented in Section~\ref{sec:level}. %Lastly, we discuss the genericity of the layering principle in Section~\ref{sec:discussion} which can, for example, also be used to display multi source content.

\section{About notation}\label{sec:notation}

From the phonetic alphabet for speech to the musical score for music, notation consists in putting together on a one or two-dimensional space symbols describing specific sound events. In a manner probably inherited from writing, time sequencing is usually depicted from left to right in the Western musical culture. Specific to the musical score is the use of the vertical axis to depict the pitch. A musical tone is therefore solely described in terms of time of appearance, duration, pitch and sometimes intensity. As such, the score is largely prescriptive and gives a tremendous amount of freedom to the musical performer in terms of interpretation.

In an intent to provide a more descriptive notation of musical objects, Schaeffer~\cite{Schaeffer1966} designed a "solf\`ege des object musicaux" that extensively apprehend the description of any kind of sound object. Perhaps because of its complexity this notation is hardly used today. In an effort to simplify this notation, Schafer proposed a notational system that can be considered for describing any kind of sound, be it a unique event or any kind of compound. The main rationale is to split the temporal axis from left to right into 3 parts corresponding to the \textit{attack}, \textit{sustain} and \textit{decay}. For each part, its duration, frequency (related to the notion of mass as introduced by Schaeffer), fluctuations (related to the notion of grain as introduced by Schaeffer) and dynamics are displayed from top to bottom. Except for the frequency content that is depicted as a rough spectrogram contour, the other dimensions are described according to a specific alphabet of a few symbols. An example taken from~\cite{Schafer1977} of such annotation is given on Figure~\ref{fig:notation} for the sound of a church bell.

\begin{figure}
\begin{tabular}{|c|c|c|c|}
\hline
& Attack & Body & Decay \\
\hline
Duration & moderate & non-existent & slow \\
\hline
Frequency  & \multicolumn{3}{c|}{steady low} \\
\hline
Fluctuations  & transient & \multicolumn{2}{c|}{steady-state} \\
\hline
Dynamics  & \multicolumn{3}{c|}{loud to soft} \\
\hline
Duration & \multicolumn{3}{c|}{ $\longleftarrow$ 3 seconds $\longrightarrow$ } \\
\hline
\end{tabular}
\caption{Annotation of a church bell from Schafer~\cite{Schafer1977}.}
\label{fig:notation}
\end{figure}


\section{About measure}\label{sec:physicist}

When dealing with sound as a physicist, one wants to quantify mechanical properties and display them precisely. As in notation, the main aspect that is commonly looked for is the distribution of energy across frequency and time. The distribution of energy as a function of the modulation rate and the frequency scale of observations are less considered in the signal processing literature~\cite{Chi2005a, Anden2011} but  are shown to be  perceptually important~\cite{dau1997modeling, yang1992auditory}.

Therefore, in order to display a sound on a two-dimensional plane, one has to resort to a choice or a compromise. Either timing is emphasized and frequency neglected as in the waveform display~\ref{fig:waveform} or frequency is emphasized  and timing neglected  as in the display of the Fourier spectrum~\ref{fig:spectrum}. A compromise is made by considering time and frequency respectively as horizontal and vertical axes of the two-dimensional plane as with the popular spectrogram \textit{magnitude of the short term Fourier transform}, see Figure~\ref{fig:spectrogram}. In such display, the use of a color code conveys information about energy.  %In most papers in signal processing, the color code ranges from blue (low energy) to red (high energy). Even though it enhances contrast, it also contradicts with the data-ink principle introduced by Tufte in~\cite{Tufte1983}. Indeed, as most spectra are sparse, the display is covered by large sections of blue which are non informative.

\begin{figure}[!h]
  \subfloat[Waveform]
  {\includegraphics[width=\linewidth]{churchBell_waveform}
\label{fig:waveform}} \par
\subfloat[Spectrum]
{
\includegraphics[width=\linewidth]{churchBell_spectrum}
\label{fig:spectrum}} \par
\subfloat[Spectrogram]
{
\includegraphics[width=\linewidth]{churchBell_spectrogram}
\label{fig:spectrogram}
}
\caption{Standard displays of the sound of a church bell.}
\label{fig:standard}

\end{figure}

That said, we believe that the spectrogram display still favors frequency over time. Spectral structure can be analyzed precisely, for example harmonicity, modulations, etc. Conversely, temporal dynamics and structure are harder to appreciate, as the way energy fluctuates in each sub bands has to be reconstructed from the color code.

The spectrogram is a display that is thus in our opinion very powerful for close inspection of a sound event that is active over a short period of time. Indeed, enlarging the time resolution quickly blurs the frequency resolution and may lead to a completely non informative display.

\section{Visualizing spectral content using stacked graph}\label{sec:spack}

\begin{figure*}
  \begin{center}
\begin{tikzpicture}[box/.style={draw,rounded corners,text width=2.5cm,align=center}, node distance=1.5cm]
\node[box] (a) {Mel scaled magnitude spectrogram};
\node[left=of a,font=\bfseries] (aux1) {} ;
\node (b) [box,right=of a,node distance=10cm] {Stacking};
\node[right=of b,font=\bfseries] (c) {};
\draw[->] (aux1) -- node [above] {Audio} (a);
\draw[->] (a) -- node [above] {} (b);
\draw[->] (b) -- node [above] {spack} (c);
\end{tikzpicture}
\end{center}
\caption{Processing chain of the spack display.}
\label{fig:schema}
\end{figure*}

With those limitations in mind, we propose in this paper to take a compromise that conversely favors time over frequency. In such display, the plane is therefore organized with time and energy as the horizontal and vertical axes respectively. The frequency is displayed as stacked layers displaying the level of energy across frequency sub bands of growing frequency range. Those layers can have colors assigned.

We seek a display that depicts information that is perceptually meaningful. Therefore, we consider spectral data projected on a Mel-scale~\cite{Stevens1937}.%\footnote{This step would not be meaningful for people interested in biology (bats vocalizations for example). In this case, the perceptual front end can be disregarded.}.% and each sub bands is optionally corrected for equal loudness~\cite{Robinson1956} with cubic root compression. % optionally? Or optimally?

In order to improve legibility, colors are assigned to frequency layers according to their ranges with a color code ranging from blue (low frequency) to yellow (high frequency).
The blue color is often associated with large phenomena, with the following adjectives: celestial, calm, deep, whereas the yellow color is often associated with transient phenomena that are highly energetic. Kandinsky in~\cite{Kandinsky1954} states that "Blue is comparable to low pitched organ sounds. Yellow becomes high pitched and can not be very deep". The color code is then chosen to be a linear gradient from blue (low frequency range) through green (middle frequency range) to yellow (high frequency range). In this paper, the gradient follows the LCH color model specified by the Commission Internationale de l'\'Eclairage (CIE) so that the perceived brightness appears to change uniformly across the gradient while maintaining the color saturation. %This color scale was the best compromise we were able to find, though the proportion of blue and yellow is still not satisfying, leading to a graph that is not well balanced in terms of color contrast. %The natural conversion in gray scale is to map blue to black and yellow to white. A black and white display can be achieved with a stress on the 3 spectral ranges, see Figure~\ref{fig:spack_bw}. % Je ne vois pas bien en quoi bleu est ``physiquement'' connecte a noir et jaune a blanc -- apres tout, le bleu correspond a rayonnement electromagnetique de plus haute energie. Globalement, la convention choisie est a l'inverse du physique (on connecte les basses frequences sonores aux hautes frequences electromagnetiques et vice-versa), du coup je trouverais plus prudent d'eviter de faire des references physiques.

We argue that this display, termed spectral stack (spack), convey useful information about the sound. In particular, it conveys nicely, aside of fine details, the important dimensions retained by Schafer, see Figure~\ref{fig:notation}.

To compute the spack display, a mel-scaled magnitude spectrogram is computed from the audio, see Figure~\ref{fig:schema}. To each mel spectral band is assigned a given color code from dark blue (low frequency) to yellow (high frequency). At each time frame, the spack display is a stacking of the magnitude values of each mel frequency band, see Figure~\ref{fig:spack}.

\begin{figure*}[t]
\includegraphics[width=\linewidth]{churchBell_spack}
\caption{Spectral stack display (spack) of the sound of a church bell. The color code conveys nicely the modulation within each frequency band and the overall disappearance of the high frequency range.}
\label{fig:spack}
\end{figure*}


\section{Task 1: Identifying the musical instrument} \label{sec:instrument}


\begin{figure}[ht!]
\includegraphics[width=\linewidth]{instrumentResults}
\caption{Classification performance of the different displays on Task 1 (identifying the musical instrument): sound (S)  waveform (W), spectrogram (Spe) and spack (Spa). The star shows the average performance and the length of the vertical line is twice the standard deviation.}
\label{fig:instrument}
\end{figure}

The identification of the musical instrument used to play a tone rely largely on 2 factors, the spectral envelope and the attack~\cite{grey1977multidimensional, agus2012fast}.  The spack display shall be able to conveniently display those factors. Indeed, the spectral envelope, \textit{i.e.} the distribution of the energy across frequency is encoded using the stacking axis and color code. The attack is also well displayed as the spack focuses on the display of energy through time.

\subsection{Protocol}

Several tones played by four musical instruments: piano, violin, trumpet, and flute are considered as stimuli. Each instrument is played \textit{mezzo forte} at 5 different pitches: C, D, E, F and G. For each sound, three visual representations are evaluated: waveform (W), spectrogram (Spe) and spack (Spa). For reference, the sound (S) is also considered\footnote{The sounds and the visual displays are available on the companion website}.

The test is a forced-choice categorization task. The sounds are displayed by gray dots on a 2 dimensional plane displayed on a computer screen. The dots can be moved freely within this plane and colored using 4 different colors, each corresponding to a given instrument. The correspondence is given to the subjects at the beginning of the experiment by the instructor: piano (black), violin (red), trumpet (magenta), and flute (green). If the sound modality is tested, the sound is played when the dot is clicked. If a visual modality is tested, the corresponding display is shown when the dot is clicked using the mouse.

Eight subjects, studying at the Engineering school "Ecole Centrale de Nantes", aged from 24 to 26 years, performed the test. Each subject reported normal hearing. They performed the test at the same time in a quiet environment using headphones. The sound level was set to a comfortable level before the experiment. A short introduction was given by the instructor for each display with a focus on the meaning of the axes and the color code. The subjects performed the evaluation using the sound modality first. The order of the three remaining modalities are ordered randomly among subjects to reduce the impact of precedence. The test is over when the subjects have assigned a color to each dot, this for all the evaluated modalities.

\subsection{Results}

Classification performance is evaluated as the number of couple of sounds played by the same instrument that have been assigned the same color divided by the number of couples. As can be seen on Figure~\ref{fig:instrument}, the task is trivial when listening to the sound, as the subjects achieve perfect classification. On overall, the classification is quite good for each of the graphical displays with a higher average performance for the spack display. Subjects verbally reported ease of use for the spack display.

\section{Task 2: Assessing the level of a saxophone performance} \label{sec:level}

\begin{figure}[t]
  \subfloat[Waveform]
  {\includegraphics[width=\linewidth]{christophe_B_3_waveform}
} \par
\subfloat[Spectrogram]
{
\includegraphics[width=\linewidth]{christophe_B_3_spectrogram}
} \par
\subfloat[Spack]
{
\includegraphics[width=\linewidth]{christophe_B_3_spack}
}
\caption{Graphical displays of \textit{forte} B tone. Several performance issues can be observed: lack of airflow control at the attack, change of pitch and loudness at 3 seconds and lack of steady airflow during the whole performance.}
\label{fig:low}
\end{figure}

\begin{figure}[t]
  \subfloat[Waveform]
  {\includegraphics[width=\linewidth]{francesco_G_3_waveform}
} \par
\subfloat[Spectrogram]
{
\includegraphics[width=\linewidth]{francesco_G_3_spectrogram}
} \par
\subfloat[Spack]
{
\includegraphics[width=\linewidth]{francesco_G_3_spack}
}
\caption{Graphical displays of another \textit{forte} B tone. Several performance issues can be observed, for example: lack of sharpness at the attack, change of timbre and loudness at 5 seconds.}
\label{fig:high}
\end{figure}

The control of the breath while playing the saxophone is crucial and can be monitored to assess the technical level of a saxophone player~\cite{robine2006evaluation}. For example, playing a single tone with sharp attack and constant amplitude during the steady state is non trivial and requires years of practice.

Professional players typically practice such exercises on a daily basis as warm-ups and perform them with a trainer to get criticisms in order to improve their skills. Using graphical displays of their performance could be useful for them to spot during or after the performance. In order to be efficient, such display shall be intuitive with a few degrees of freedom in order to be easy to understand.

The validation of the spack display for such pedagogical needs is out of the scope of this paper. Nonetheless, we designed here a task that can demonstrate how several meaningful characteristics of the saxophone performance can be identified only by considering the graphical displays under evaluation.

In this kind of training, it could be useful for the trainer to have some kind of display of its performance. As the crucial part is to be able to control the air flow while playing in order to keep a stable amplitude and timbre, we hypothesize that the spack display may be a good candidate for such a task.

\subsection{Protocol}

The stimuli considered in this experiment are recorded performances of four saxophone players with a technical level assumed to be high or low (2 low, 2 high). Each player played several tones at pitch B and G. They were asked to play each note in three different ways: \textit{piano}, \textit{forte} and \textit{crescendo decrescendo}\footnote{The sounds and the visual displays are available on the companion website}.

The test follows a XXY structure, where three performances are shown to the subject, one is at a given level (high or low) and the other two of the other level (low or high). The subject is then asked, based solely on the modality at hand, to select the one that is different from the two others. 24 triplets are randomly selected from the valid combinations of the above described stimuli.

16 subjects, studying at the Engineering school "Ecole Centrale de Nantes", aged from 24 to 28 years, performed the test in two sessions, 9 for the first session, and 7 for the second session. Each subject reported normal hearing. For each session, they performed the test at the same time in a quiet environment using headphones. The sound level was set to a comfortable level before the experiment. A short introduction was given by the instructor for each display with a focus on the meaning of the axis and the color code. The subjects performed the evaluation using the sound modality first. The order of the three remaining modalities are ordered randomly among subjects to reduce the impact of precedence. The test is over when the subjects have examined the 24 triplets for the 4 evaluated modalities.

\subsection{Results}


\begin{figure}[t]
\includegraphics[width=\linewidth]{levelBox}
\caption{Boxplot display of the differentiation performance of the different displays on Task 2 (detecting the level of the saxophone player): sound (S),  waveform (W), spectrogram (Spe) and spack (Spa).}
\label{fig:level}
\end{figure}

For each modality, the number of correct selection is averaged among the 24 triplets and then averaged among subjects. As can be seen on Figure~\ref{fig:level}, the task is more complex than task 1, as the score achieved using the sound modality is lower than task 1. This might be due to the fact that the task is less explicit than task 1. For the visual displays, the same ranking as task 1 is observed with a larger difference between each modality.

\begin{table}[t]
  \caption{Results of the repeated measure ANOVA evaluating the effect of the type of display on the performance. \label{tab:anova}}
\begin{tabular}{cccccc}
 & sum sq. & df & mean sq. & F & p-value \\
 \hline
 Type & 0.13 & 3  & 0.045 & 5.3 & 0.003 \\
 Error & 0.38 & 45 & 0.008 \\
\end{tabular}
\end{table}

A repeated measure ANOVA is used to test the potential significance of the type of display on the differentiation performance. A mauchly test reveals that the default of sphericity is not significant, thus no correction of the degrees of freedom of the Fisher test is needed. Table~\ref{tab:anova} presents the results of the Fisher test showing that the effect of the representation is significant $p=0.003$. In addition, a multiple comparison test shows that the only significant differences are between Waveform and Spack $p=0.03$ and Waveform and Sound $p=0.003$. No significant difference is found between the remaining modalities: the Sound, the Spectrogram and the Spack displays.

Thus, if considering the graphical displays solely, only the Spack displays significantly improves upon the Waveform display. As can be seen on Figure~\ref{fig:level}, The spectrogram display have the largest dispersion of correct answer rate, \textit{i.e.} the ratio of correct responses over the number of possible responses, termed p(c) in the following. Considering the distribution of p(c) for the spectrogram display shown on Figure~\ref{fig:hist}, two modes can be observed contrary to the one of the spack display. Even though each subjects have been given the same introduction to each of the graphical displays, their familiarity with the standard displays may vary since some subjects had previous training in signal processing courses. This may explain the higher mode in the distribution of the spectrogram display.  Even if this observation shall be considered with care due to the rather low number of subjects, this can lead us to conjecture about the influence of the familiarity of the subjects with the spectrogram display on the reported performance. The spack display does not exhibit the same distribution profile and prior familiarity cannot be assumed as the display was equally new to all subjects.

\begin{figure}[ht!]
  \includegraphics[width=\linewidth]{levelHist}
\caption{Histogram of the classification performance for the spectrogram (Spe) and the spack (Spa) displays. Only the spectrogram display exhibit two modes, suggesting different levels of expertise of the subjects.}
\label{fig:hist}
\end{figure}

% \begin{figure}[ht!]
%   \subfloat[Spectrogram]
%   {\includegraphics[width=\linewidth]{levelHistSpe}} \par
%   \subfloat[Spectrogram]
%   {\includegraphics[width=\linewidth]{levelHistSpa}}
% \caption{Histogram of the classification performance for the spectrogram and the spack displays. Only the spectrogram display exhibit two modes, suggesting different levels of expertise of the subjects.}
% \label{fig:hist}
% \end{figure}

\section{Conclusions}\label{sec:discussion}

%Visualizing at the same time a large number of sound sources is hard to achieve. Most Digital Audio Workstations (DAWs) have their displays set as a vertical array of waveforms, loosing a lot of space and reducing the ability of the user to interact with different sources that are far apart in the array.

%Considering stacked graphs to display information about audio data is not commonly used.

In this paper, we proposed a display based on the stacking of the envelopes of logarithmically spaced band pass filters. We have shown qualitatively that this kind of display may have some potential as it conveys nicely the distribution of the energy across time and frequency in a way that is an alternative to the one taken when considering the spectrogram. %The so called spack display can thus be considered as an alternative.

When considering two evaluation tasks: 1) identifying the type of instrument played, and 2) identifying at which skill level a saxophone tone is played, the spack display compares favorably to more conventional displays, such as the waveform and spectrogram displays. Subjects reported ease of understanding and quick access to important aspects of the sounds.

Future work will focus on the design of validation tasks for the spack display using a wider range of audio data, namely speech and environmental data.

As the spack display is both compact and intuitive, it can be considered as an inspection tool while practicing a musical instrument in order to monitor the control of the nuance and the timbre while playing. Evaluation of the spack display in such a training use case would thus be of interest.

%Besides frequency based stacking, many other mapping of information may also be considered. In order to display multi sources audio data for example, one can stack the envelope of the sources to be displayed
%, see Figure~\ref{fig:smack}
%.
%This kind of display allows the user to quickly grasp the overall organization of the sound scene at the cost of a distortion of the envelope of sound display on the top most levels. This distortion can be minimized in many different ways~\cite{Byron2008}, but we found that sorting the sources according to their overall energy is a simple and effective heuristic. By doing so, the low amplitude sounds are less distorted while the high energy ones are severely distorted but the surface is still legible.

% \begin{figure}[t]
% %  \subfloat[Color display]{
% \includegraphics[width=\linewidth]{Multisource_smack}
% \label{fig:smack_color}
% %}
% %\subfloat[Gray scale display]{
% %\includegraphics[width=.5\linewidth]{Multisource_smack_gray}
% %\label{fig:smack_gray}}
% \caption{Sound multiple stack (smack) display of an environmental soundscape.}
% \label{fig:smack}
% \end{figure}

%While considering such a display for sound manipulation, one could use the bottom of the graph to put the specific source to be edited. That way, the user can conveniently edit this source without distortion of display while keeping on eye on the evolutions of the other tracks.




% \section{Visualizing multi channel content (scack)}\label{sec:scack}
%
% Another setting where such a style of display can be useful is to display multiple channels. On Figure~\ref{fig:scack} is shown an arrangement of the 6 channels of a 5.1 setting. The color code is chosen in order to use hue to convey panning information and luminance to convey depth. The subwoofer is assigned to black as it is an omnidirectional source. In gray scale or black and white, the display is still legible due to the use of the vertical axis to convey panning information.
% %, see respectively Figures~\ref{fig:scack_gray} and~\ref{fig:scack_bw}.
%
% \begin{figure}[ht]
% \begin{center}
% \includegraphics[width=.5\linewidth]{51SceneSynth_scack}
% \caption{}
% \label{fig:scack}
% \end{center}
% \end{figure}

%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width=.5\linewidth]{51SceneSynth_scack_gray}
%\caption{}
%\label{fig:scack_gray}
%\end{center}
%\end{figure}
%
%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width=.5\linewidth]{51SceneSynth_scack_bw}
%\caption{}
%\label{fig:scack_bw}
%\end{center}
%\end{figure}



\section{Acknowledgments}

The authors would like to acknowledge support for this project
from ANR project Houle (grant ANR-11-JS03-005-01) and ANR project Cense (grant ANR-16-CE22-0012).

\bibliography{bib}


\end{document}
%
% \subsection{Visualizing melodic content}\label{sec:schack}
%
% This display may also be used to represent the chroma, a feature widely used to describe musical signals. In our implementation, chroma is obtained by warping the spectral content of the signal into the well tempered western music scale of 12 semi-tones. % `wildly' used, haha -- ``Chromas gone wild''
% %As for the SPACK (see~\ref{sec:spack}) We use a 2D representation with time on the horizontal axis and energy on the vertical axis.
% In this display, termed Sound CHroma staCK (SCHACK), each chroma is described by a stacked layer of a carefully chosen color. % Mais si c'est vraiment 'wild', on peut aussi l'appeler SHAG
%
% To set a meaningful color map, we got our inspiration from the color-tonality association made by Scriabine~\cite{Galeyev2001}. Scriabine was a synesthete who extensively experimented the relationship between sounds and colors. He made an color-tonality association in which two tones which are in close proximity in the cycle of fifth are represented which similar colors. Considering that Scriabine's choice of colors was subjective, and the associations he made was between tones and colors and not notes and colors\cite{Galeyev2001}. As a chroma is more related to the notion of note than the notion of tone, we chose a more common color set while maintaining the color mapping based on the cycle of fifths. \\
%
% %% Je ne comprends pas ``ton'' ici. Si tu veux dire ``gamme'', c'est plutot ``scale''
% To represent the 12 notes of the scale, we use 12 colors in a HSV color space (\textit{Hue}, \textit{Saturation}, \textit{Value}),  each of them having the same \textit{Saturation} and \textit{Value} and differ only in their \textit{Hue}.  The 12 colors of our HSV space are mapped onto the 12 notes of the musical scale ordered in a cycle of fifths, see Figure~\ref{fig:cycleQuinte}. Doing so ensures that two consecutive chroma (\textit{ie.} semi-tones in that case) are represented with distinct colors, which are helpful for a stack based representation. Furthermore this color-map is  adapted to illustrate both unique notes and chords.
%
% If we consider a single note, the color-map allows us to represent the first four partials of the note, which are the octave, the Pythagorean  fifth (which can be considered as the perfect fifth of the fundamental frequency  to within a few comma), and double octave of the fundamental frequency, with similar colors having regards to their saturation. All the others partials are represented with distinct colors (see figure~\ref{fig:fluteGammeMonoNorm_schack}).
%
% If we consider a chord, the color-map allows us to represent two "consonant" notes (fifth, fourth, major second intervals) with two closed colors  in term of saturation and two "dissonant" notes (Tritone: \textit{Diabolus in musica} or minor second intervals) with well distinct colors, in the case that of the spectral energy is contained in the fundamental frequency. Those notions of "consonant" and "dissonant" have a significant relevance to the Western tonal music theory. Third and sixth intervals may be also considered "consonant" interval, but as the color mapping is based on the cycle of fifths, third and sixth intervals are not represented with particular colors associations.
%
% Let us consider a musical example. Figure~\ref{fig:duoFluteMono_schack} show the schack representation of the beginning of a flute \textit{Duett} composed by Georg Philipp Telemann (see figure~\ref{fig:telemann} for the score). The envelop of the stacked layers clearly illustrate that 1) the amplitude is modulated and  2) four notes (\textit{F}) are played with more intensity than the other notes. The color map allows us to distinguish the third first notes (\textit{B-flat} \textit{D} \textit{E-flat}) by identifying the broader layers, as most spectral energy is concentrated on their fundamental frequency and their first partial (octave). We can see that the \textit{B-flat} is maintained during the notes \textit{D} \textit{E-flat} as the thickness of the layer corresponding to  \textit{B-flat} remains important.  As the first three notes are relatively far from each others in the cycle of fifths, they are represented with distinct colors. For the fourth note (\textit{F}), the fact that the layers corresponding to the two adjacent semi tones (\textit{E-flat} and \textit{F-sharp}) are presents may be due to a lack of selectivity in the frequency analysis.
% % (VOIR AVEC MATHIEU POURQUOI ON A CET EFFET DE DEBORDEMENT SUR LES SEMI TONS ADJACENTS: largeur du pic).
% For all the notes (\textit{F}), we can see that the amplitude of the third partials (Pythagorean  fifth, which would correspond to the note \textit{C}) is important. Every time the note \textit{F} is played, the red layer is broader. \\
%
% Considering the first chord (\textit{E-flat}/\textit{F}) and the last chords (\textit{A}/\textit{F}), we can see that le layer of \textit{E-flat} is more important for the first chord (\textit{E-flat}/\textit{F}) than it is for the second chord (\textit{A}/\textit{F}), in which the layer of \textit{A} is broader.
%
%
% \begin{figure}[t]
% \centering
% \includegraphics[width=\linewidth]{cycleQuinte.pdf}
% \caption{Proposed mapping between HSV color space and the notes ordered in a cycle of fifths.}
% \label{fig:cycleQuinte}
% \end{figure}
%
%
% \begin{figure}[t]
% \centering
% \includegraphics[width=\linewidth]{fluteGammeMonoNorm_schack}
% \caption{Sound chroma stack (shack) representation of a full musical scale of 12 semi-tones played by a flute.}
% \label{fig:fluteGammeMonoNorm_schack}
% \end{figure}
%
% \section{Discussion}\label{sec:discussion}
%
% We introduced in this paper an interesting set of displays. For mono channel data, the spectral and chromatic displays allows the user to display frequency related information on a time / energy plane, thus nicely conveying information about the variation of energy trough time. For multi sources or multichannel data, the proposed displays allow the user to display a large amount of information on a single graph.
%
% Even though the displays are designed to be meaningful in a large set of applications, some settings are application dependent. Whether or not a compression shall be applied typically depends on the type of data to be analyzed. For speech data, it leads to much better display, whereas for many environmental sounds it may degrade the timing information. An horizontal smoothing using a gaussian kernel is applied in order to reduce high frequency variations that would blur the visual display. The size of the kernel typically depends on the duration of the audio but also on the style of display.
%
%
% \begin{figure*}
%   \subfloat[Musical score]
%   {
% \includegraphics[width=.6\linewidth]{telemann}
% \label{fig:telemann}} \par
% \subfloat[Sound chroma stack (schack) display]
% {
% \includegraphics[width=\linewidth]{duoFluteMono_schack}
% \label{fig:duoFluteMono_schack}}
% \caption{Notation and schack display of the extract of a flute \textit{Duett} by George Philipp Telemann (1681-1767)}
% \label{fig:schack}
% \end{figure*}
