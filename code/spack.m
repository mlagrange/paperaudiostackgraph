function [] = spack(fileName, monochrom, pngFileName)

addpath('rastamat');

if ~exist('monochrom', 'var'), monochrom = 1; end
if ~exist('pngFileName', 'var'), pngFileName = ['../paper/figures/churchBellMel.png'];end
if ~exist('fileName', 'var'), fileName = 'sounds/churchBell.wav'; end

% time/frequency analysis
hopTime = 0.01;
[s, sr] = audioread(fileName);
[~, as, ps, asFrequency] = melfcc(s, sr, 'hoptime', hopTime, 'wintime', .02, 'nbands', 10);
timeAxis = (1:size(ps, 2))*hopTime;
% time smoothing
as = gaussianSmoothing(as, 80)';
as=as/max(sum(as, 2))*max(abs(s));

% display
clf
hold on
if ~monochrom
    colormap(blue2yellow(size(as, 1)));
    h = area(timeAxis, as, 'EdgeColor', 'none', 'LineStyle','none');
    plot(timeAxis, sum(as, 2),'LineWidth', 1, 'color', 'k');
    c = colorbar('location', 'westoutside', 'ytick', 1+.4+(0:size(as, 2))/(size(as, 2)+1)*size(as, 2), 'yticklabel', ceil(asFrequency(2:end-1)));
    ylabel(c, 'Frequency (Hz)')
else
    colormap(ones(size(as, 2), 3));
    h = area(timeAxis, as, 'EdgeColor', 'k', 'LineStyle',':');
    plot(timeAxis, sum(as(:, 1:ceil(end/3)), 2),'LineWidth', 1, 'color', 'k');
    plot(timeAxis, sum(as(:, 1:ceil(2*end/3)), 2),'LineWidth', 1, 'color', 'k');
    plot(timeAxis, sum(as, 2),'LineWidth', 1, 'color', 'k');
end
set(gca,'YTick', [  ]);
axis([0 timeAxis(end) 0 1]);

% export
if ~isempty(pngFileName)
    set(gcf, 'PaperPositionMode', 'auto');
    print ('-dpng', '-r300', pngFileName);
end


function as = gaussianSmoothing(as, factor)

if ~exist('factor', 'var'), factor = 200; end

% gaussian filtering for smooth display
% may be adapted the size to the length of the file
g = gausswin(ceil(size(as, 2)/factor));
g = g/sum(g);
for k=1:size(as, 1)
    as(k, :) = conv(as(k, :), g, 'same');
end


